import React, { Component } from 'react';
import PaginatedTable from './PaginatedTable';



class App extends Component {

    /**
     * Demo actions to be passed into the rowActions.
     * 
     * rowActions will be passed into the datatable
     * will be triggered on the row it is clicked on.
     */
    rowActions = [ 
        { 
            name: 'Delete', 
            action: (themeName) =>  alert(`DELETE action called on ${themeName}`)
        },
        { 
            name: 'Edit', 
            action: (themeName) => alert(`EDIT action called on ${themeName}`)
        }
    ];

    
    /**
     * Table Columns to be passed into the datatable.
     */
    tableColumns = [
        { name: "Status", key: "status", type: "badge" },
        { name: "Theme Name", key: "name", type: "text" },
        { name: "Developer", key: "developer", type: "text" },
        { name: "Date Added", key: "dateAdded", type: "text" },
    ];


    /**
     * Some data to pass into the datatable. The data displayed 
     * will depend on the tableColumn keys defined above, and 
     * will position the values under the name assosicated with the key.
     */
    tableData = [
        { status: "Approved", name: "Enfold", developer: "Kriesi", dateAdded: "08/11/2019" },
        { status: "Approved", name: "X | The Theme", developer: "THEMECO", dateAdded: "08/11/2019" },
        { status: "Approved", name: "Avada", developer: "ThemeFusion", dateAdded: "08/11/2019" },
        { status: "Approved", name: "Salient", developer: "ThemeNectar", dateAdded: "08/11/2019" },
        { status: "Ready", name: "BeTheme", developer: "muffingroup", dateAdded: "08/11/2019" },
        { status: "Ready", name: "Flatsome", developer: "UX-themes", dateAdded: "07/11/2019" },
        { status: "Queued", name: "Uncode", developer: "undsgn", dateAdded: "07/11/2019" },
        { status: "Queued", name: "Porto", developer: "p-themes", dateAdded: "07/11/2019" },
        { status: "Queued", name: "Impreza", developer: "UpSolution", dateAdded: "07/11/2019" },
        { status: "Queued", name: "The7", developer: "Dream-Theme", dateAdded: "06/11/2019" },
        { status: "Queued", name: "SoleDad", developer: "PencilDesign", dateAdded: "06/11/2019" },
        { status: "Queued", name: "Kalium", developer: "Laborator", dateAdded: "06/11/2019" },
        { status: "Queued", name: "Jupiter", developer: "artbees", dateAdded: "06/11/2019" },
        { status: "Queued", name: "Newspaper", developer: "tagDiv", dateAdded: "06/11/2019" },
        { status: "Queued", name: "Malina", developer: "ArtstudioWorks", dateAdded: "06/11/2019" },
    ]


    /**
     * Default render method
     */
    render() {
        return (
            <div style={{ backgroundColor: "#eeeeee", minHeight: '100vh' }}>
                <div className="container" style={{ paddingTop: 40 }}>
                    <div className="card shadow" style={{ border: 0, borderRadius: "10px !important" }}>
                        <div className="card-body" style={{ backgroundColor: "#ffffff" }}>

                            <PaginatedTable 
                                currentPage={1} 
                                resultsPerPage={6} 
                                tableColumns={this.tableColumns} 
                                rowActions={this.rowActions}
                                tableData={this.tableData} 
                            />

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



export default App;