import React from 'react';



/**
 * The badge component to be displayed in the datatable.
 * 
 * @param {string} label The label of the badge. Must be one from this component.
 */
const StatusBadge = ({ label }) => {
    switch(label) {
        case 'Pending':
            return <span class="badge badge-light">{label}</span>;
        case 'Queued':
            return <span class="badge badge-secondary">{label}</span>;
        case 'Searching':
            return <span class="badge badge-warning">{label}</span>;
        case 'Ready':
            return <span class="badge badge-primary">{label}</span>;
        case 'Approved':
            return <span class="badge badge-success">{label}</span>;
        default:
            return <span class="badge badge-danger">{label || "error"}</span>;
    }
}



export default StatusBadge;