import React, { Component } from 'react';
import PaginationButtons from './PaginationButtons';
import TableRowActionsButton from './TableRowActionsButton/TableRowActionsButton';
import StatusBadge from './StatusBadge';



/**
 * Datatable for displaying large amounts of data 
 * and easily calling actions on rows.
 */
class PaginatedTable extends Component {
    state = {
        currentPage: 1,
        resultsPerPage: 10
    }


    /**
     * Lifecycle method. 
     * Runs once after the component mounts.
     */
    componentDidMount() {
        this.setState({ 
            currentPage: this.props.currentPage,
            resultsPerPage: this.props.resultsPerPage
        });
    }

    /**
     * Returns an array where the first value is the lowest visible 
     * entry, and the second value is the highest entry visible.
     * 
     * First value starts at 0.
     */
    getCurrentEntryRange = () => {
        const entriesStart = (this.state.currentPage-1) * this.state.resultsPerPage;
        const entriesEnd = (this.state.currentPage*this.state.resultsPerPage <= this.props.tableData.length) 
            ? this.state.currentPage*this.state.resultsPerPage 
            : this.props.tableData.length;

        return [entriesStart, entriesEnd];
    }


    /**
     * Function to be passed to children for updating this
     * component's current page.
     */
    onPageChange = (newPage) => {
        this.setState({ currentPage: newPage });
    }


    /**
     * Handles the rendering of the header row of the table.
     */
    renderTableHeader = () => {
        // Render a column header for each column provided
        let formattedColumns = this.props.tableColumns.map(column => <th key={column.name} >{column.name}</th>);

        // If actions, add action header
        if (this.props.rowActions) {
            formattedColumns.push(<th key="Actions" >Actions</th>);
        }

        // Return formatted header
        return <tr>{formattedColumns}</tr>;
    }


    /**
     * Handles the rendering of the body within the table.
     */
    renderTableBody = () => {
        const tableData = this.props.tableData;
        const columns = this.props.tableColumns;
        const entryRange = this.getCurrentEntryRange();
        let formattedRows = [];

        // Get visible rows
        const visibleEntries = tableData.slice(entryRange[0], entryRange[1]);

        // For each visible row
        visibleEntries.map(row => {

            // For each cell
            let formattedCells = columns.map(column => {

                // Format cell based on provided type
                switch (column.type) {
                    case 'badge':
                        return <td><StatusBadge label={row[column.key]} /></td>;
                    default:
                        return <td>{row[column.key]}</td>;
                };

            });

            // If actions, add the actions to end of row
            if (this.props.rowActions) {
                formattedCells.push(<td key={row.name}><TableRowActionsButton rowItemName={row.name} actions={this.props.rowActions} /></td>);
            }

            // Add formatted row to formatted rows
            formattedRows.push(<tr key={row.name}>{formattedCells}</tr>);
        });

        // Return all formatted rows
        return formattedRows;
    }


    /**
     * Handles the rendering of a row within the table body.
     */
    renderRow = (row) => {
        const formattedRow = row.map(cell => <td>{cell}</td>);
        return <tr>{formattedRow}</tr>;
    }


    /**
     * Renders the details of the datatable such as total number of results 
     * and what subset of results are being shown.
     */
    renderResultsDetails = () => {
        const entryRange = this.getCurrentEntryRange();
        if (this.state.entriesTotal !== 0) {
            return <p>{`Showing ${entryRange[0]+1} to ${entryRange[1]} of ${this.props.tableData.length} entries`}</p>;
        }
        return <p>No data present</p>;
    }
    

    /**
     * The default render method.
     */
    render() {
        return(
            <div className="datatable">
                <table className="table" style={{ borderBottom: "1px solid #dee2e6" }}>
                    <thead class="thead">
                        {this.renderTableHeader()}
                    </thead>
                    <tbody>
                        {this.renderTableBody()}
                    </tbody>
                </table>
                <div class="float-left">
                    {this.renderResultsDetails()}
                </div>
                <PaginationButtons
                    currentPage={ this.state.currentPage } 
                    numberOfResults={ this.props.tableData.length }
                    resultsPerPage={ this.props.resultsPerPage } 
                    onPageChange={ this.onPageChange } 
                    float="right"
                />
            </div>
        );
    }

}



export default PaginatedTable;