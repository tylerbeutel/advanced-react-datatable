import './TableRowActionsButton.css';
import React from 'react';



/**
 * The action button in the table row.
 * Used for easily selecting an action to be performed on the row.
 * 
 * @param {string} rowItemName - The row name to be passed into action
 * @param {Object[]} actions - Action object
 * @param {string} actions[].name - Action label to be displayed in options
 * @param {function} actions[].action - Action function to be called on row
 */
const TableRowActionsButton = ({ rowItemName, actions }) => {

    
    /**
     * Turns list of actions into a dropdown list.
     * 
     * @param {actions} actions 
     */
    const renderActionOptions = actions => {
        return actions.map(action => {
            return <a class="dropdown-item" href="#" onClick={ () => action.action(rowItemName) }>{action.name}</a>
        });
    }

    return (
        <div className="dropdown">
            <button 
                className="btn btn-secondary dropdown-toggle action-button" 
                type="button" 
                id="dropdownMenuButton" 
                data-toggle="dropdown" 
                aria-haspopup="true" 
                aria-expanded="false"
            >
                <i className="fas fa-ellipsis-h"></i>
            </button>
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                {renderActionOptions(actions)}
            </div>
        </div>
    );
}



export default TableRowActionsButton;