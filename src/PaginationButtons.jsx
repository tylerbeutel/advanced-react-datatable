import React, { Component } from 'react';


/**
 * Pagination buttons for handling page controlling logic.
 * 
 * @param {Number} currentPage - The active page number.
 * @param {Number} numberOfResults - Total number of results.
 * @param {Number} resultsPerPage - Number of results to be shown on each page.
 * @param {Function} onPageChange - Function to be called on page change.
 * @param {String} [float] - Direction to float the pagination.
 */
class PaginationButtons extends Component {

    /**
     * Calculates what numbers should be visible in the pagination
     * based on the current page number and the total number of results.
     */
    calcVisibleButtonNumbers = () => {
        const numberOfResults = this.props.numberOfResults;
        const resultsPerPage = this.props.resultsPerPage
        const currentPage = this.props.currentPage;
        const numberOfPages = Math.ceil( numberOfResults / resultsPerPage );
        
        let visibleNumbers = [];

        /* Add first page */
        visibleNumbers.push({ 
            label: "First", 
            value: 1,
            disabled: currentPage === 1,
            active: false
        });
        /* Numbers before active */
        for (let i=currentPage-1; i<currentPage; i++) {
            if (1<=i && i<currentPage) {
                visibleNumbers.push({
                    label: i, 
                    value: i,
                    disabled: false, 
                    active: false 
                });
            }
        }
        /* Active number */
        visibleNumbers.push({ 
            label: currentPage, 
            value: currentPage,
            disabled: false,
            active: true
        });
        /* Numbers after active */
        for (let i=currentPage+1; i<currentPage+2; i++) {
            if (currentPage<i && i<=numberOfPages) {
                visibleNumbers.push({ 
                    label: i, 
                    value: i,
                    disabled: false, 
                    active: false 
                });
            }
        }
        /* Add last page */
        visibleNumbers.push({ 
            label: "Last", 
            value: numberOfPages,
            disabled: currentPage === numberOfPages,
            active: false
        });

        return visibleNumbers;
    }

    
    /**
     * Handles the rendering of the pagination buttons.
     */
    renderPaginationButtons = () => {
        const visibleNumbers = this.calcVisibleButtonNumbers();

        return visibleNumbers.map(button => {
            const disabled = button.disabled ? 'disabled' : '';
            const active = button.active ? 'active' : '';

            return(
                <li className={`page-item ${disabled} ${active}`}>
                    <a style={{ cursor: 'pointer' }} onClick={ 
                        () => this.props.onPageChange(button.value) 
                    } className="page-link">{button.label}</a>
                </li>
            );
        });
    }


    /**
     * Default render method.
     */
    render() {

        return (
            <nav aria-label="Page navigation" style={{ float: this.props.float || 'none' }}>
                <ul className='pagination'>
                    {this.renderPaginationButtons()}
                </ul>
            </nav>
        );
    }
}



export default PaginationButtons;